# pysource

A silly solution to a silly problem.

## Summary

This all started with [a question on StackOverflow](https://stackoverflow.com/questions/63356387/applying-source-on-python-script-to-export-environment-variables).
Basically the question is trying to replace the somewhat common practice of exporting variables from a bash script with the apparently impossible practice of exporting variables from a *Python* script.

This script does that.

## Usage

Download the `pysource` script and put it somewhere you can execute files, i.e.

```bash
$ curl -o pysource https://gitlab.com/afiorillo/pysource/-/raw/master/pysource
$ chmod +x pysource && mv pysource /usr/local/bin/pysource
```

Then you need to `source` pysource pointing to your script.
For example:

```bash
$ cat my_python_vars.py
FOO = 'bar'
$ source pysource my_python_vars.py
$ echo $FOO
bar
```

## Caveats

This section is non-comprehensive and there are likely to be limitations not mentioned here.

- In order to support `__file__` arguments, a temporary file is created with the original script plus the footer. As a result, `__file__` doesn't actually point to the original script, but the original script appended with the `.temp` suffix.
