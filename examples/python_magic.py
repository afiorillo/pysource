#!/usr/bin/env python

# file magic
from pathlib import Path
THIS_FILES_DIR = str(Path(__file__).resolve().parent)

# weird maths
from math import sin
from datetime import datetime
MATHEMATICAL_RESULT = sin(datetime.utcnow().timestamp())
